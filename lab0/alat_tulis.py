harga_pulpen = 10000
harga_pensil = 5000
harga_cortape = 15000

print("Toko ini menjual:")
print("1. Pulpen " + str(harga_pulpen) + "/pcs")
print("2. Pensil " + str(harga_pensil) + "/pcs")
print("3. Cortape " + str(harga_cortape) + "/pcs")

print("Masukkan jumlah barang yang ingin anda beli")
jmlh_pulpen = int(input("Pulpen: "))
jmlh_pensil = int(input("Pensil: "))
jmlh_cortape = int(input("Cortape: "))

print("Ringkasan pembelian")
a = jmlh_pulpen * harga_pulpen
b = jmlh_pensil * harga_pensil
c = jmlh_cortape * harga_cortape
total = a+b+c
print(f"{jmlh_pulpen} pulpen x {harga_pulpen}")
print(f"{jmlh_pensil} cortape x {harga_pensil}")
print(f"{jmlh_cortape} cortape x {harga_cortape}")
print(f"Total harga: {total}")