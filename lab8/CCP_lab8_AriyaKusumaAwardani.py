class KueUlangTahun:
    def __init__(self, tipe, harga, tulisan, angka_lilin, topping):
        self.__tipe = tipe
        self.__harga = harga
        self.__tulisan = tulisan
        self.__angka_lilin = angka_lilin
        self.__topping = topping
    
    def get_tipe(self):
        return self.__tipe

    def get_harga(self):
        return self.__harga
    
    def get_tulisan(self):
        return self.__tulisan
    
    def get_angka_lilin(self):
        return self.__angka_lilin
    
    def get_topping(self):
        return self.__topping


class KueSponge(KueUlangTahun):
	def __init__(self, tipe, tulisan, angka_lilin, topping, rasa, warna_frosting, harga = 2500):
		super().__init__(tipe, harga, tulisan, angka_lilin, topping)
		self.__rasa = rasa
		self.__warna_frosting = warna_frosting
		self.__harga = harga

	def get_rasa(self):
		return self.__rasa

	def get_warna_frosting(self):
		return self.__warna_frosting

	def get_harga(self):
		return self.__harga
    

class KueKeju(KueUlangTahun):
	def __init__(self, tipe, tulisan, angka_lilin, topping, jenis_kue_keju, harga=3000):
        super().__init__(tipe, tulisan, angka_lilin, topping)
		self.__jenis_kue_keju = jenis_kue_keju
		self.__harga = harga

	def get_jenis_kue_keju(self):
		return self.__jenis_kue_keju

	def get_harga(self):
		return self.__harga


class KueBuah(KueUlangTahun):
	def __init__(self, tipe, tulisan, angka_lilin, topping, jenis_kue_buah, harga=3500):
        super().__init__(tipe, tulisan, angka_lilin, topping)
		self.__jenis_kue_buah = jenis_kue_buah
		self.__harga = harga
    
	def get_jenis_kue_buah(self):
		return self.__jenis_kue_buah

	def get_harga(self):
		return self.__harga


# Fungsi ini harus diimplementasikan!
def buat_custom_bundle():
    print("""Jenis kue:
1. Kue Sponge
2. Kue Keju
3. Kue Buah""")

    tipe = input("Pilih tipe kue: ")

    if tipe == "1":
        rasa = input("Pilih rasa (coklat/stroberi): ")
        warna_frosting = input("Pilih warna frosting (coklat/pink): ")
        topping = input("Pilih topping (Ceri/Stroberi): ")
        angka_lilin = input("Masukkan angka lilin: ")
        tulisan = input("Masukkan tulisan pada kue: ")
        return KueSponge("Kue Sponge " + rasa, tulisan, angka_lilin, topping, warna_frosting, "¥3000")

    elif tipe == "2":
        jenis_kue = input("Pilih jenis kue keju (New York/Japanese):")
        topping = input("Pilih topping (Ceri/Stroberi): ")
        angka_lilin = input("Masukkan angka lilin: ")
        tulisan = input("Masukkan tulisan pada kue: ")
        return KueKeju("Kue Keju " + jenis_kue, tulisan, angka_lilin, topping, "¥32000")

    elif tipe == "3":
        jenis_kue = input("Pilih jenis kue buah (American/British): ")
        topping = input("Pilih topping (Ceri/Stroberi): ")
        angka_lilin = input("Masukkan angka lilin: ")
        tulisan = input("Masukkan tulisan pada kue: ")
        return KueBuah("Kue Buah " + jenis_kue, tulisan, angka_lilin, topping, "¥3500")


# Fungsi ini harus diimplementasikan!
def pilih_premade_bundle():
    print("""Pilihan paket istimewa:
1. New York-style Cheesecake with Strawberry Topping
2. Chocolate Sponge Cake with Cherry Topping and Blue Icing
3. American Fruitcake with Apple-Grape-Melon-mix Topping""")

    paket = input("Pilih paket: ")
    angka_lilin = input("Masukkan angka lilin: ")
    tulisan = input("Masukkan tulisan pada kue: ")

    if paket == "1":
        return KueUlangTahun("Kue Keju New York", "¥2700", tulisan, angka_lilin, "Stroberi")

    elif paket == "2":
        return KueUlangTahun("Kue Spons Coklat", "¥2200", tulisan, angka_lilin, "ceri dan gula biru")

    elif paket == "3":
        return KueUlangTahun("Kue Buah Amerika", "¥3100", tulisan, angka_lilin, "Apel-Anggur-Melon")

# Fungsi ini harus diimplementasikan!
def print_detail_kue(kue):
    return print(kue.get_tipe() + " dengan topping " + kue.get_topping() + "\nTulisan ucapan yang anda inginkan adalah " + kue.get_tulisan() + "\nAngka lilin yang anda pilih adalah: " + kue.get_angka_lilin() + "\nHarga: " + kue.get_harga())
   

# Fungsi main jangan diubah!
def main():
    print("Selamat datang di Homura!")
    print("Saat ini sedang diadakan event khusus bertema kue ulang tahun.")

    is_ganti = True

    while is_ganti:
        print("\nBundle kue yang kami sediakan: ")
        print("1. Bundle pre-made")
        print("2. Bundle custom\n")

        pilihan_bundle = input("Pilih bundle: ")

        kue = None

        while True:
            if pilihan_bundle == "1":
                kue = pilih_premade_bundle()
                break
            elif pilihan_bundle == "2":
                kue = buat_custom_bundle()
                break
            else:
                print("Pilihan anda tidak valid.")
                pilihan_bundle = input("Pilih paket: ")
        
        print("\nBerikut adalah kue pesanan anda: ")

        print_detail_kue(kue)

        while True:
            ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

            if ganti == "Ya":
                break
            elif ganti == "Tidak":
                is_ganti = False
                break
            else:
                print("Pilihan anda tidak valid.")
                ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")
    
    print("\nTerima kasih sudah berbelanja di Homura!")

if __name__ == "__main__":
    main()