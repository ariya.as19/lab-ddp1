try:
    #user input
    file_input = input("Masukkan nama file input: ")
    file_output = input("Masukkan nama file output: ")

    #membuka file input
    filein = open(file_input, "r")
    lines1 = filein.readlines()

    #menghitung line sebelum
    line_filein = 0
    for line in lines1:
        if line != "\n":
            line_filein += 1
    
    #menutup file input
    filein.close()
    
    pesankotor = ""
    pesanasli = ""
    
    #menghitung line sesudah
    line_fileout = 0
    
    #menghapus line yang berisikan total angka ganjil
    for line1 in lines1:
        s = sum(int(char) for char in line1 if char.isnumeric())
        if s%2 == 0:
            pesankotor += line1
            line_fileout += 1
        else:
            continue
    
    #membentuk pesan asli
    for i in pesankotor:
        if i.isalpha():
            if i.isupper():
                pesanasli += "\n" + i
            else:
                pesanasli += i
        elif i == " ":
            pesanasli += i

    #menghapus "\n" di awal
    pesanasli = pesanasli.strip()
    #membuat file output
    fileout = open(file_output, "w+")
    
    #Menulis pesan ke file output
    fileout.write(pesanasli)
    
    print(f"\nTotal baris dalam file input: {line_filein}")
    print(f"Total baris yang disimpan: {line_fileout}")
    

    fileout.close()

except FileNotFoundError:
    #exception jika nama file tidak ada
    print("Nama file yang anda masukkan tidak ditemukan :(")