#User input dan mengubah variable type str ke int
karakter_masukan = input("Masukkan karakter: ")
angka = ord(karakter_masukan)

#Mengencrypted variable angka
encrypted = angka % 7 * (angka ** 2) % 69 + 666
print(f"Fake Eligma untuk {karakter_masukan} : {encrypted}")

#mengubah variable encrypted dari base 10 ke base 2 dan 16
bin_x = bin(encrypted)
hex_x = hex(encrypted)

#Menghilangkan dua karakter pertama
new_bin_x = bin_x[2:]
new_hex_x = hex_x[2:]

#hasil akhir
hasil = new_bin_x + new_hex_x
print(f"Enkripsi Fake Eligma untuk {karakter_masukan} : {hasil}")