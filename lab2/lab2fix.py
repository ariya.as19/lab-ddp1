hpe = 100
hpr = 100
round = 1
batas = "*" * 26
while True:
    hp = f"Hp Ei: {hpe}\nHp Raiden: {hpr}\nPutaran ke {round}\n{batas}\nApa yang ingin anda lakukan?\n1. Menyerang\n2. Menghindar\n3. Musou no Hitachi"
    hp3 = hp[:-21]
    if round % 3 != 0:
        print(hp)
        user_input = int(input("Masukkan pilihan anda: "))
        

        if user_input == 1:
            hpe -= 20
            hpr -= 20
            round += 1
            print(f"""Ei menyerang Raiden dan mengurangi Hp Raiden sebesar 20\nRaiden menyerang Ei dan mengurangi Hp Ei sebesar 20\n{batas}""")
        elif user_input == 2:
            round += 1
            print(f"""Raiden menyerang namun Ei berhasi menghindar\n{batas}""")
        elif user_input == 3 :
            hpe -= 20
            hpr -= 50
            round += 1
            print(f"""Ei menyerang Raiden dengan "Musou no Hitachi" dan mengurangi Hp Raiden sebesar 50\nRaiden menyerang Ei dan mengurangi Hp Ei sebesar 20\n{batas}""")

           
    else: 
        print(hp)
        print("Raiden bersiap menggunakan \"The Final Calamity\"")
        user_input = int(input("Masukkan pilihan anda: "))
        

        if user_input == 1:
            hpe -= 50
            hpr -= 20
            round += 1
            print(f"""Ei menyerang Raiden dan mengurangi Hp Raiden sebesar 20\nRaiden menyerang Ei dan mengurangi Hp Ei sebesar 20\n{batas}""")
        elif user_input == 2:
            round += 1
            print(f"""Raiden menyerang namun Ei berhasi menghindar\n{batas}""")
        elif user_input == 3 :
            hpe -= 50
            hpr -= 50
            round += 1
            print(f"""Ei menyerang Raiden dengan "Musou no Hitachi" dan mengurangi Hp Raiden sebesar 50\nRaiden menyerang Ei dan mengurangi Hp Ei sebesar 20\n{batas}""")
    

    if hpe > 0 and hpr <= 0:
        print("Ei memenangkan pertarungan")
        break
    if hpe <= 0 and hpr > 0:
        print("Raiden memenangkan pertarungan")
        break
    if hpe == 0 and hpr == 0:
        print("Pertarungan seri")
        break