import numpy


intro = """Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang
dapat dilakukan:
1. Penjumlahan
2. Pengurangan
3. Transpose
4. Determinan
5. Keluar"""


while True:
    print(intro)
    pilih_operasi = input("Silakan pilih operasi: ")

    if pilih_operasi=="1":
        ukuran_matriks = input("Ukuran matriks: ")
        matriks1 =[]
        if ukuran_matriks[0].isdigit():
            for i in range(1, int(ukuran_matriks[0])+1):
                lst_baris = []
                input_baris1 = input(f"Baris {i} matriks 1: ")
                lst_baris = input_baris1.split()
                lst_baris = [int(i) for i in lst_baris]
                matriks1 += [lst_baris]

        matriks2 =[]
        if ukuran_matriks[2].isdigit():
            for i in range(1, int(ukuran_matriks[2])+1):
                input_baris2 = input(f"Baris {i} matriks 2: ")
                lst_baris = []
                input_baris1 = input(f"Baris {i} matriks 1: ")
                lst_baris = input_baris1.split()
                lst_baris = [int(i) for i in lst_baris]
                matriks2 += [lst_baris]

        x = numpy.array(matriks1)
        y = numpy.array(matriks2)
        print (numpy.add(x,y))
        
            

    elif pilih_operasi=="2":
        ukuran_matriks = input("Ukuran matriks: ")
        matriks1 =[]
        if ukuran_matriks[0].isdigit():
            for i in range(1, int(ukuran_matriks[0])+1):
                lst_baris = []
                input_baris1 = input(f"Baris {i} matriks 1: ")
                lst_baris = input_baris1.split()
                lst_baris = [int(i) for i in lst_baris]
                matriks1 += [lst_baris]

        matriks2 =[]
        if ukuran_matriks[2].isdigit():
            for i in range(1, int(ukuran_matriks[2])+1):
                input_baris2 = input(f"Baris {i} matriks 2: ")
                lst_baris = []
                input_baris1 = input(f"Baris {i} matriks 1: ")
                lst_baris = input_baris1.split()
                lst_baris = [int(i) for i in lst_baris]
                matriks2 += [lst_baris]

        x = numpy.array(matriks1)
        y = numpy.array(matriks2)
        print (numpy.subtract(x,y))
    elif pilih_operasi=="3":
        ukuran_matriks = input("Ukuran matriks: ")
        matriks1 =[]
        if ukuran_matriks[0].isdigit():
            for i in range(1, int(ukuran_matriks[0])+1):
                lst_baris = []
                input_baris1 = input(f"Baris {i} matriks 1: ")
                lst_baris = input_baris1.split()
                lst_baris = [int(i) for i in lst_baris]
                matriks1 += [lst_baris]

        

        x = numpy.array(matriks1)
        
        print (numpy.add(x,y))
    elif pilih_operasi=="4":
        continue
    elif pilih_operasi=="5":
        print("Sampai Jumpa!")
        break